<?php
/*
/*
Plugin Name: iFleet Shipping Rates for Woocommerce
Plugin URI: http://icommerce.asia
Description: iCommerce Shipping Rates.
Author: iCommerce Asia
Version: 1.0
Author URI: http://icommerce.asia
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_action( 'woocommerce_shipping_init', 'ifleet_shipping_method_init' );
add_filter( 'woocommerce_shipping_methods', 'register_ifleet_method' );
add_filter( 'woocommerce_package_rates', 'hide_shipping_when_free_is_available', 10, 2 );
// To do: Get the info from shipping.icommerce.asia
add_filter( 'woocommerce_cart_shipping_method_full_label', 'ifleet_delivery_time',10,2 );

function ifleet_shipping_method_init() {
    if ( ! class_exists( 'WC_Shipping_iFleet' ) ) {
        require_once 'class-shipping-ifleet.php';
    }
}

function register_ifleet_method( $methods ) {
    $methods[ 'ifleet' ] = new WC_Shipping_iFleet();
    return $methods;
}

function CallAPI($method, $url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}

function hide_shipping_when_free_is_available( $rates ) {
    $free = array();
    foreach ( $rates as $rate_id => $rate ) {
        if ( 'free_shipping' === $rate->method_id ) {
            $free[ $rate_id ] = $rate;
            break;
        }
    }
    return ! empty( $free ) ? $free : $rates;
}


function ifleet_delivery_time($label, $method){
    $label .= '<br><small>';
    $label .= 'Est. Delivery: 3 - 5 Days';
    $label .= '<br></small>';
    return $label;
}
