<?php
class WC_Shipping_iFleet extends WC_Shipping_Method {

    /**
    * Constructor. The instance ID is passed to this.
    */
    public function __construct( $instance_id = 0 ) {
        $this->id                    = 'ifleet';
        $this->instance_id 			     = absint( $instance_id );
        $this->method_title          = __( 'iFleet' );
        $this->method_description    = __( 'iFleet' );
        $this->supports              = array(
            'shipping-zones',
            'instance-settings',
        );

        $this->form_fields = array(
            'enabled' => array(
                'title' => __( 'Enable/Disable' ),
                'type' => 'checkbox',
                'label' => __( 'Enable this shipping method' ),
                'default' => 'yes',
                ),
            'title' => array(
                'title' => __( 'iFleet' ),
                'type' => 'text',
                'description' => __( 'This controls the title which the user sees during checkout.' ),
                'default' => __( 'iFleet' ),
                'desc_tip' => true
                ),
            'package_wt' => array(
                'title' => __( 'Weight of packaging (kg)' ),
                'type' => 'text',
                'description' => __( 'Weight of packaging in kg' ),
                'default' => 0.3,
                'desc_tip' => true
                )
        );
        $this->enabled = $this->get_option( 'enabled' );
        $this->title = $this->get_option( 'title' );
        $this->package_wt = $this->get_option( 'package_wt' );

        add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );

    }

    /**
    * calculate_shipping function.
    * @param array $package (default: array())
    */

    public function calculate_shipping( $package = array() ) {

        $destination = $package['destination']['country'];
        $weight = $this->getWeightOfParcel($package);
        list($cost,$delivery_time) = $this->getCost($weight, $destination);
        if($cost > 0){
            $this->add_rate( array(
            'id'    => $this->id . $this->instance_id,
            'label' => $this->title,
            'cost'  => $cost,
        ));
        }
    }

    function getWeightOfParcel($package = array()){
        $weight = 0;
        $dimension = 0;
        $updated_wt = 0;
        $weight_unit = (get_option('woocommerce_weight_unit') ? get_option('woocommerce_weight_unit') : 'kg');
        $dimension_unit = (get_option('woocommerce_dimension_unit') ? get_option('woocommerce_dimension_unit') : 'cm');

        //convert unit to kg
        if($weight_unit == 'g'){
            $wt_multiply = 0.001;
        } elseif($weight_unit == 'kg') {
            $wt_multiply = 1;
        } elseif($weight_unit == 'lbs') {
            $wt_multiply = 0.45;
        } elseif($weight_unit == 'oz') {
            $wt_multiply = 0.028;
        }

        //convert dimension to cm
        if($dimension_unit == 'm'){
            $dm_multiply = 100;
        } elseif($dimension_unit == 'cm'){
            $dm_multiply = 1;
        } elseif($dimension_unit == 'mm'){
            $dm_multiply = 0.1;
        } elseif($dimension_unit == 'in'){
            $dm_multiply = 2.54;
        } elseif($dimension_unit == 'yd'){
            $dm_multiply = 91.44;
        }

        foreach ( $package['contents'] as $item_id => $values ) {
            $_product  = $values['data'];
            $weight = $_product->get_weight() * $wt_multiply;
            $dimension = $_product->length  * $_product->width * $_product->height * $dm_multiply / 1000;
            $updated_wt = $updated_wt + (max($weight,$dimension) * $values['quantity']);
        }
        $updated_wt = $updated_wt + $this->package_wt;
        return $updated_wt;
    }

    function getCost($updated_wt,$destination){
        $cost = 0;
        $info = CallAPI('GET','http://shipping.icommerce.asia/get-rate?weight='.$updated_wt.'&destination='.$destination);
        $info = json_decode($info);
        $delivery_time = '';
        if(isset($info->rate)){
            $cost = $info->rate;
        }
        if(isset($info->delivery_time)){
            $delivery_time = $info->delivery_time;
        }

        return array($cost,$delivery_time);
    }
}